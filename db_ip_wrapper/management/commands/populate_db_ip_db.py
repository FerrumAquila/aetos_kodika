import csv

from django.core.management.base import BaseCommand
from django.conf import settings

from db_ip_wrapper.models import DbIpLookup


class Command(BaseCommand):
    def populate_data(self):
        DbIpLookup.objects.all().delete()
        file_path = settings.CSV_FILE_PATH
        print file_path
        with open(file_path, 'rb') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=' ', quotechar='|')
            for row in csv_reader:
                new_row = "".join(row)
                if '::' not in new_row:
                    # print ">>>>row"
                    # print new_row
                    row_data = new_row.replace('"', '').split(',')
                    # print ">>>>row_data"
                    # print row_data
                    # location = "_".join(row_data[2:]).decode('utf-8')
                    try:
                        db_ip_obj = DbIpLookup(
                            ip_start=row_data[0].decode('utf-8'),
                            ip_end=row_data[1].decode('utf-8'),
                            country=row_data[2].decode('utf-8'),
                            stateprov=row_data[3].decode('utf-8'),
                            city=row_data[4].decode('utf-8'),
                        )
                        db_ip_obj.save()
                        # print db_ip_obj
                    except UnicodeEncodeError:
                        print ">>>>row"
                        print new_row
                        print ">>>>row_data"
                        print row_data
                        # from IPython.frontend.terminal.embed import InteractiveShellEmbed
                        # InteractiveShellEmbed()()

    def handle(self, *args, **options):
        self.populate_data()
