__author__ = 'ironeagle'

from django.db import models
from django.db.models import Avg, Count, F, Max, Min, Sum, Q
from django.core.exceptions import ObjectDoesNotExist
from netaddr import *
from .utils import ip_is_good


class DbIpLookup(models.Model):
    ip_start = models.CharField(primary_key=True, max_length=16)
    ip_end = models.CharField(max_length=16)
    country = models.CharField(max_length=2)
    stateprov = models.CharField(max_length=80)
    city = models.CharField(max_length=80)

    def __unicode__(self):
        return "IPs in '%s' - '%s' belongs to '%s'" % (self.ip_start, self.ip_end, self.city)

    @classmethod
    def get_city(cls, ip_add):
        if ip_is_good(ip_add):
            ip = IPAddress(ip_add)
            probables = DbIpLookup.objects.filter(
                Q(ip_start__startswith=".".join(ip_add.split(".")[:3])) |
                Q(ip_end__startswith=".".join(ip_add.split(".")[:3]))
            ) or DbIpLookup.objects.filter(
                Q(ip_start__startswith=".".join(ip_add.split(".")[:2])) |
                Q(ip_end__startswith=".".join(ip_add.split(".")[:2]))
            ) or DbIpLookup.objects.filter(
                Q(ip_start__startswith=".".join(ip_add.split(".")[:1])) |
                Q(ip_end__startswith=".".join(ip_add.split(".")[:1]))
            )
            for ip_range in [IPRange(probable.ip_start, probable.ip_end) for probable in probables]:
                if ip in ip_range:
                    return DbIpLookup.objects.get(ip_start=str(ip_range).split('-')[0], ip_end=str(ip_range).split('-')[1])
            raise ObjectDoesNotExist("IP '%s' isn't mapped to any City" % ip_add)
        raise TypeError("'%s' is an incorrect IP" % ip_add)