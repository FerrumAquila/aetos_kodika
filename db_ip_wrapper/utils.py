__author__ = 'ironeagle'


def ip_is_good(ip_add):
    chunks = ip_add.split('.')
    for chunk in chunks:
        if int(chunk) > 255:
            return False
    return True