# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DbIpLookup',
            fields=[
                ('ip_start', models.CharField(max_length=16, serialize=False, primary_key=True)),
                ('ip_end', models.CharField(max_length=16)),
                ('country', models.CharField(max_length=2)),
                ('stateprov', models.CharField(max_length=80)),
                ('city', models.CharField(max_length=80)),
            ],
        ),
    ]
