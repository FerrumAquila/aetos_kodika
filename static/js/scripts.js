/* window resize - start */
$(".section").height($(window).height())
$(".scroll-down").eq(0).offset({left: ($(".scroll-down").eq(0).offset().left - $(".scroll-down").eq(0).children().width()/2)})

$( window ).resize(function() {
  $(".section").height($(window).height())
  $(".scroll-down").eq(0).offset({left: ($(".scroll-down").eq(0).offset().left - $(".scroll-down").eq(0).children().width()/2)})});
/* window resize - end */


$(document).ready(function(){

//$('body,html').css("overflow","hidden")

/* smooth scrolling for scroll to top - start */
$(".scroll-top").click(function(){
  $("body, html").animate({scrollTop:0},800);
})
/* smooth scrolling for scroll to top - end */


/* smooth scrolling for scroll down - start */
scrollDown = function () {
  for(i=$(".section").length;i<$(".section").length*2;i++){
    //console.log(i % $(".section").length)
    if($(".section").eq(i % $(".section").length).attr("id") == "currentScroll"){
      $(".section").eq(i % $(".section").length).attr("id", "lol")
      $(".section").eq((i+1) % $(".section").length).attr("id", "currentScroll")
      //console.log((i+1) % $(".section").length)
      break
    }

    //console.log($("#currentScroll"))
  }
  //console.log($(".section").eq((i+1) % $(".section").length).attr("class"))
  //console.log((i+1) % $(".section").length + ", " + i)
  /*
  if($(".section").eq((i+1) % $(".section").length).css("background-color") == "rgb(254, 254, 254)"){
    //console.log("loop 1")
    //console.log("white")
    $("#main-nav").css("color", "black")
  }else{
    if($(".section").eq((i+1) % $(".section").length).css("background-color") == "rgba(0, 0, 0, 0)"){
      //console.log("loop 2")
      //console.log("white")
      $("#main-nav").css("color", "black")
    }else{
      if($(".section").eq((i+1) % $(".section").length).css("background-color") == "rgb(255, 255, 255)"){
        //console.log("loop 3")
        $("#main-nav").css("color", "black")
        //console.log("white")
      }else{
        //console.log("managable")
        $("#main-nav").css("color", "white")
      }
    }
  }
  */
  var bg_color = $("#currentScroll").children().children().children().css("background-color")
  //console.log(bg_color)
  $("#fbNavBar").css("background-color", bg_color)
  $("body,html").animate({scrollTop: $("#currentScroll").offset().top },1000, function(){
    shouldScroll = true
    console.log("Should Scroll " + shouldScroll)
  });
};

scrollUp = function () {
  for(i=$(".section").length;i<$(".section").length*2;i++){
    //console.log(i % $(".section").length)
    if($(".section").eq(i % $(".section").length).attr("id") == "currentScroll"){
      $(".section").eq(i % $(".section").length).attr("id", "lol")
      $(".section").eq((i-1) % $(".section").length).attr("id", "currentScroll")
      //console.log((i-1) % $(".section").length)
      break
    }
    //console.log($(".section"))
  }
  //console.log($(".section").eq((i-1) % $(".section").length).attr("class"))
  //console.log((i-1) % $(".section").length + ", " + i)
  /*
  if($(".section").eq((i-1) % $(".section").length).css("background-color") == "rgb(254, 254, 254)"){
    //console.log("loop 1")
    //console.log("white")
    $("#main-nav").css("color", "black")
  }else{
    if($(".section").eq((i-1) % $(".section").length).css("background-color") == "rgba(0, 0, 0, 0)"){
      //console.log("loop 2")
      //console.log("white")
      $("#main-nav").css("color", "black")
    }else{
      if($(".section").eq((i-1) % $(".section").length).css("background-color") == "rgb(255, 255, 255)"){
        //console.log("loop 3")
        $("#main-nav").css("color", "black")
        //console.log("white")
      }else{
        //console.log("managable")
        $("#main-nav").css("color", "white")
      }
    }
  }
  */
  var bg_color = $("#currentScroll").children().children().children().css("background-color")
  //console.log(bg_color)
  $("#fbNavBar").css("background-color", bg_color)
  $("body,html").animate({scrollTop: $("#currentScroll").offset().top }, 1000, function(){
    shouldScroll = true
    //console.log("Should Scroll " + shouldScroll)
  });
};

$(".scroll-down").click(function(){
  scrollDown()
})

$(document).keydown(function(e) {
    switch(e.which) {
        case 37:
        scrollUp()

        case 38:
        scrollUp()
        break;

        case 87:
        scrollUp()

        case 65:
        scrollUp()
        break;

        case 39:
        scrollDown()
        break;

        case 40:
        scrollDown()
        break;

        case 68:
        scrollDown()
        break;

        case 83:
        scrollDown()
        break;

        default:
        //console.log(e.which)
        return;
    }
    e.preventDefault();
});

(function(c){var a=["DOMMouseScroll","mousewheel"];c.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var d=a.length;d;){this.addEventListener(a[--d],b,false)}}else{this.onmousewheel=b}},teardown:function(){if(this.removeEventListener){for(var d=a.length;d;){this.removeEventListener(a[--d],b,false)}}else{this.onmousewheel=null}}};c.fn.extend({mousewheel:function(d){return d?this.bind("mousewheel",d):this.trigger("mousewheel")},unmousewheel:function(d){return this.unbind("mousewheel",d)}});function b(f){var d=[].slice.call(arguments,1),g=0,e=true;f=c.event.fix(f||window.event);f.type="mousewheel";if(f.wheelDelta){g=f.wheelDelta/120}if(f.detail){g=-f.detail/3}d.unshift(f,g);return c.event.handle.apply(this,d)}})(jQuery);

if (document.addEventListener){
  document.addEventListener("mousewheel", MouseWheelHandler(), false);
  document.addEventListener("DOMMouseScroll", MouseWheelHandler(), false);
}else{
  sq.attachEvent("onmousewheel", MouseWheelHandler());
}

shouldScroll = true
//console.log("Should Scroll " + shouldScroll)
function MouseWheelHandler() {
  return function (e) {
    // cross-browser wheel delta
    e123 = window.event || e123;
    delta = Math.max(-1, Math.min(1, (e123.wheelDelta || -e123.detail)));

    //scrolling down?
    if (delta < 0) {
      //scrollDown()
      //console.log(e123.timeStamp)
      if(shouldScroll){
        //console.log("scrolling down")
        shouldScroll = false
        scrollDown()
        //console.log("Should Scroll " + shouldScroll)
      }
    }

    //scrolling up?
    else {
      //scrollUp()
      //console.log(e123.timeStamp)
      if(shouldScroll){
        //console.log("scrolling up")
        shouldScroll = false
        scrollUp()
        //console.log("Should Scroll " + shouldScroll)
      }
    }
    return false;
  }
}

/* smooth scrolling for scroll down - end */


/* highlight the top nav as scrolling occurs - start */
$("body").scrollspy({ target: "#navbar" })
/* highlight the top nav as scrolling occurs - end */


/* opacity on mouse move - start */
timerAppear = 20
timerDisappear = 1000

var hidden, fadenav, dimNav = function () {
    hidden = true;
    $("#main-nav").animate({"opacity": 0.5}, timerDisappear);
};

$(function () {
    dimNav();
    $("body").on("mousemove", function (e) {
        if (hidden) {
            $("#main-nav").animate({"opacity": 1}, timerAppear);
            hidden = false;
        }
        if (fadenav !== null) {
            clearTimeout(fadenav);
        }
        fadenav = setTimeout(dimNav, timerDisappear);
    });
});

$("#main-nav").on("mousemove",function(e){
    if(hidden){
        $("#main-nav").animate({"opacity": 1}, timerAppear);
        hidden = false;
    }
    clearTimeout(fadenav);
    e.stopPropagation();
});
/* opacity on mouse move - end */


});