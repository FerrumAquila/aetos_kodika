__author__ = 'ironeagle'


import json
from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from oauth2client.client import credentials_from_code, FlowExchangeError

import utils as app_utils
from models import Credential, Mail
from forms import UploadAttachmentForm
from decorators import gmail_auth_required


@login_required
@gmail_auth_required
def home(request):
    messages = Mail.objects.filter(user=request.user).exclude(from_contact__email=request.user.email).exclude(
        from_contact=None)
    return render(request, "e-app-home.html", {
        "messages": messages,
    })


@login_required
def login(request):
    return render(request, "login.html")


@login_required
@gmail_auth_required
def compose(request, msg_id=None):
    attachment_form = UploadAttachmentForm(request)
    context = {"attachment_form": attachment_form}
    if msg_id:
        mail = Mail.objects.get(message_id=msg_id)
        to = ",".join([email.email for email in mail.from_contact.all()])
        context.update({
            "to": to,
            "msg_id": msg_id,
        })
    return render(request, "e-app-compose.html", context)


@never_cache
@login_required
def registration(request):
    try:
        credentials = credentials_from_code(
            client_id=settings.CONFIG['GOOGLE_API_CLIENT_ID'],
            client_secret=settings.CONFIG['GOOGLE_API_CLIENT_SECRET'],
            scope='https://mail.google.com/',
            code=request.GET['code'],
            redirect_uri=settings.CONFIG['GOOGLE_API_REDIRECT_URI']
        )
        auth_success = True
        credential_obj, created = Credential.objects.get_or_create(user=request.user)
        credential_obj.credentials = json.loads(credentials.to_json())
        credential_obj.save()
    except FlowExchangeError, e:
        print "Google Exception", e
        auth_success = False

    print "Auth Success", auth_success
    return redirect(home)


@login_required
@gmail_auth_required
def get_unread(request):
    Credential.objects.get(user=request.user).fetch_mails()
    messages = [mail.data for mail in Mail.objects.filter(user=request.user)]
    return JsonResponse({
        "success": True,
        "objs": messages,
        "message": "Found " + str(len(messages)) + " messages",
    })


@login_required
@gmail_auth_required
def get_message(request, msg_id=None):
    if not msg_id:
        response = {
            "success": False,
            "objs": [],
            "message": "msg_id not found",
            "help": "need msg_id to get message"
        }
        return JsonResponse(response)

    if request.is_ajax():

        cred_obj = Credential.objects.filter(user=request.user)
        cred_obj = cred_obj[0]
        # print "Credentials Found " + str(cred_obj.user) + " Creds : " + str(cred_obj.credentials)
        response = cred_obj.get_service()
        service = response["action"]
        # print response

        if request.method == 'GET':
            messages = app_utils.get_message(service=service, msg_id=msg_id)
            response = {
                "success": True,
                "objs": messages,
                "message": "Found message " + str(msg_id),
                "help": "try .objs to get message objects"
            }

        else:
            response = {
                "success": False,
                "objs": [],
                "message": str(request.method) + " request not allowed",
                "help": "this url can only handle GET requests. try $.get(url)"
            }

    else:
        response = {
            "success": False,
            "objs": [],
            "message": "non ajax requests not allowed",
            "help": "this url can only handle ajax request"
        }

    return JsonResponse(response)


@csrf_exempt
@login_required
@gmail_auth_required
def send_message(request):
    if request.method == 'POST':

        email_data = json.loads(request.POST.keys()[0])
        cleaned_email_data = app_utils.clean_email_data(email_data=email_data)["email_data"]
        mail_obj = Mail()
        response = mail_obj.init_from_email_data(email_data=cleaned_email_data, user=request.user)
        print ">>>>response in send_message"
        print response
        response = mail_obj.send_mail()

    else:
        response = {
            "success": False,
            "objs": [],
            "message": str(request.method) + " request not allowed",
            "help": "this url can only handle POST requests. try $.post(url)"
        }
    return JsonResponse(response)


@csrf_exempt
@login_required
@gmail_auth_required
def reply_message(request, msg_id=None):
    if not msg_id:
        response = {
            "success": False,
            "objs": [],
            "message": "msg_id not found",
            "help": "need msg_id to reply to message"
        }
        return JsonResponse(response)

    if not Mail.objects.filter(message_id=msg_id):
        response = {
            "success": False,
            "objs": [],
            "message": "mail not found for '" + msg_id + "'",
            "help": "need mail to reply to message"
        }
        return JsonResponse(response)

    if request.method == 'POST':
        email_data = json.loads(request.POST.keys()[0])
        cleaned_email_data = app_utils.clean_email_data(email_data=email_data)["email_data"]
        # print ">>>>cleaned_email_data in reply_message"
        # print cleaned_email_data
        # print "--------------------------------------------"
        reply_mail = Mail.objects.get(message_id=msg_id).save_mail_object(reply_email_data=cleaned_email_data)
        response = reply_mail.send_mail()

    else:
        response = {
            "success": False,
            "objs": [],
            "message": str(request.method) + " request not allowed",
            "help": "this url can only handle POST requests. try $.get(url)"
        }

    return JsonResponse(response)
