__author__ = 'ironeagle'
import os
from django import forms
from models import Attachment
import fhurl


class UploadAttachmentForm(fhurl.RequestForm):
    attachment = forms.FileField(
        label='Select a file to attach',
        help_text='Max. 20 MB'
    )

    def save(self):
        attachment = self.cleaned_data["attachment"]
        filename = "_".join(attachment._name.split())
        attachment_file = Attachment(user=self.request.user, name=filename, file=attachment)
        attachment_file.save()
        file_dir = os.path.dirname(attachment_file.file.path)
        attachment_data = {
            "is_attachment": True,
            "filename": filename,
            "file_dir": file_dir,
            "attachment_id": attachment_file.id,
        }
        return {
            "message": "file saved",
            "success": True,
            "data": attachment_data,
        }
