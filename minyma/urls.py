__author__ = 'ironeagle'

import views
from forms import UploadAttachmentForm
from django.conf.urls import patterns, url
import fhurl

urlpatterns = patterns(
    '',
    # template URLs #
    # home
    url(r'^$', views.home, name='home'),

    # login
    url(r'^login/$', views.login, name='login'),

    # compose
    url(r'^compose/$', views.compose, name='compose'),

    # compose reply
    url(r'^compose/(?P<msg_id>\w+)/$', views.compose, name='compose'),


    # ajax URLs #
    # get unread messages
    url(r'^ajax/get-unread/$', views.get_unread, name='get_unread'),

    # get message
    url(r'^ajax/get-message/(?P<msg_id>\w+)/$', views.get_message, name='get_message'),

    # send message
    url(r'^ajax/send-message/$', views.send_message, name='send_message'),

    # reply message
    url(r'^ajax/reply-message/(?P<msg_id>\w+)/$', views.reply_message, name='reply_message'),


    # upload attachment
    fhurl.fhurl(r'^upload-attachment/$', UploadAttachmentForm, name='upload_attachment', json=True),
)
