import datetime
import httplib2
import json
import views as gviews

from oauth2client.client import OAuth2Credentials
from models import Credential
from django.core.urlresolvers import reverse
from django.shortcuts import redirect


def gmail_auth_required(function):
    def _inner(request, *args, **kwargs):
        cred = Credential.objects.filter(user=request.user)
        if not cred:
            return redirect(reverse(gviews.login))
        cred = cred[0]
        if not cred.credentials:
            return redirect(reverse(gviews.login))
        # user first needs to login then register
        # once user clicks on authorize in login then only we can get the 'code'

        time_left = datetime.datetime.strptime(
            cred.credentials['token_expiry'], "%Y-%m-%dT%H:%M:%SZ"
        ) - datetime.datetime.utcnow()
        if time_left.seconds < 600:
            c = cred.get_credentials()
            credentials = OAuth2Credentials(**c)
            http_obj = httplib2.Http()
            credentials.refresh(http_obj)
            cred.credentials = json.loads(credentials.to_json())
            cred.save()
        return function(request, *args, **kwargs)
    return _inner
