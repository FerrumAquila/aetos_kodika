# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
from django.conf import settings
import minyma.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AddressBook',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('data', jsonfield.fields.JSONField()),
            ],
        ),
        migrations.CreateModel(
            name='Attachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('file', models.FileField(upload_to=minyma.models.generate_filename)),
                ('data', jsonfield.fields.JSONField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Credential',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('credentials', jsonfield.fields.JSONField()),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Label',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('label_id', models.CharField(unique=True, max_length=50)),
                ('name', models.CharField(unique=True, max_length=50)),
                ('data', jsonfield.fields.JSONField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('message_id', models.CharField(max_length=200, unique=True, null=True)),
                ('subject', models.CharField(max_length=100)),
                ('snippet', models.CharField(max_length=200, null=True)),
                ('body', models.TextField(null=True, blank=True)),
                ('is_html', models.BooleanField(default=True)),
                ('is_attachment', models.BooleanField(default=False)),
                ('data', jsonfield.fields.JSONField()),
                ('msg_data', models.TextField(null=True, blank=True)),
                ('attachments', models.ManyToManyField(to='minyma.Attachment')),
                ('bcc', models.ManyToManyField(related_name='bcc_emails', to='minyma.AddressBook')),
                ('cc', models.ManyToManyField(related_name='cc_emails', to='minyma.AddressBook')),
                ('from_contact', models.ManyToManyField(related_name='from_emails', to='minyma.AddressBook')),
                ('labels', models.ManyToManyField(to='minyma.Label')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_active', models.BooleanField(default=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('thread_id', models.CharField(unique=True, max_length=100)),
                ('data', jsonfield.fields.JSONField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='mail',
            name='thread',
            field=models.ForeignKey(to='minyma.Thread', null=True),
        ),
        migrations.AddField(
            model_name='mail',
            name='to',
            field=models.ManyToManyField(related_name='to_emails', to='minyma.AddressBook'),
        ),
        migrations.AddField(
            model_name='mail',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
