__author__ = 'ironeagle'

import httplib2
from oauth2client.client import OAuth2Credentials
from apiclient.discovery import build
import logging
from BeautifulSoup import BeautifulSoup
import email_utils as eutils


# from IPython.frontend.terminal.embed import InteractiveShellEmbed
# InteractiveShellEmbed()()


def authenticate_credentials(credentials=None):
    logging.basicConfig(filename='debug.log', level=logging.DEBUG)
    credentials = OAuth2Credentials(**credentials)
    http_obj = httplib2.Http()
    http = credentials.authorize(http_obj)
    gmail_service = build('gmail', 'v1', http)
    return gmail_service


def send_mail(service, email_data, fail_on_warning=False, ignore_reply_subject=True):
    """
    if thread_id is given then subject is ignored
    to override this add ignore_reply_subject=False
    """

    response = clean_email_data(email_data, fail_on_warning=fail_on_warning)
    email_data = response["email_data"]
    if fail_on_warning:
        response.update({
            "help": "try using fail_on_warning=False or removing fail_on_warning"
        })
    # print response
    if not response["success"]:
        return response

    send_subject = email_data["subject"]

    # print "--------------------------------------------"
    # print email_data["threadId"]
    # print "--------------------------------------------"

    if email_data["threadId"] and ignore_reply_subject:
        send_thread_id = email_data["threadId"]
        response = get_thread(service=service, thread_id=send_thread_id)
        thread_message = response["messages"][0]
        send_subject = get_header_value(message=thread_message, header="Subject")
    else:
        send_thread_id = None

    if email_data["is_html"] and not email_data["is_attachment"]:
        message = eutils.CreateHTMLMessage(
            sender="me",
            to=email_data["to"],
            subject=send_subject,
            message_text=email_data["body"],
            threadId=send_thread_id
        )
    elif email_data["is_attachment"]:
        message = eutils.CreateMessageWithAttachment(
            sender="me",
            to=email_data["to"],
            subject=send_subject,
            message_text=email_data["body"],
            file_dir=email_data["file_dir"],
            filename=email_data["filename"],
            threadId=send_thread_id
        )
    else:
        message = eutils.CreateMessage(
            sender="me",
            to=email_data["to"],
            subject=send_subject,
            message_text=email_data["body"],
            threadId=send_thread_id
        )

    gmail_response = eutils.SendMessage(service=service, user_id="me", message=message)
    response.update({
        "gmail_response": gmail_response,
        "success": True,
        "message": "Mail was sent successfully\nMSG ID " + str(gmail_response["id"])
    })
    return response


def get_unread(service, complete=True, label_ids=None):
    """
    :param service:
    :param label_ids:
    :return:
    """
    default = ["UNREAD", "INBOX"]
    if label_ids:
        [label_ids.append(label) for label in default]
    else:
        label_ids = default
    message_ids = eutils.ListMessagesWithLabels(user_id="me", service=service, label_ids=label_ids)
    if complete:
        messages = [(message["id"], get_message(service=service, msg_id=message["id"])) for message in message_ids]
        return messages
    else:
        return message_ids


def get_message(service, msg_id):
    if msg_id:
        message = eutils.GetMessage(user_id="me", msg_id=msg_id, service=service)
        return message


def mark_message_read(service, msg_id):
    if msg_id:
        read_labels = eutils.CreateMsgLabels(removeLabelIds=["UNREAD"], addLabelIds=[])
        response = eutils.ModifyMessage(user_id="me", msg_id=msg_id, service=service, msg_labels=read_labels)
        return response


def mark_message_unread(service, msg_id=None):
    if msg_id:
        unread_labels = eutils.CreateMsgLabels(removeLabelIds=[], addLabelIds=["UNREAD"])
        response = eutils.ModifyMessage(user_id="me", msg_id=msg_id, service=service, msg_labels=unread_labels)
        return response
    else:
        return "Please give msg_id"


def get_header_value(message=None, header=None):
    if not header:
        return "Please give header to look for"

    if not message:
        return "Please give either message obj or message id"

    for hdr in message["payload"]["headers"]:
        if hdr["name"] == header:
            return hdr["value"]


def get_thread(service, thread_id=None):
    if thread_id:
        response = eutils.get_thread(user_id="me", service=service, thread_id=thread_id)
    else:
        return "Please give thread_id"
    return response


def clean_email_data(email_data=None, fail_on_warning=False):
    if not email_data:
        response = {
            "success": False,
            "message": "email_data missing",
            "help": "Please give email_data",
        }
        return response

    errors = []
    warnings = []

    # print ">>> Email Data before Clean up"
    # print email_data.keys()

    try:
        send_to = email_data["to"]
        if isinstance(send_to, list):
            warnings.append({
                "message": "provided a list of to emails in email_data",
                "failed": fail_on_warning,
                "key": "to",
            })
            if not fail_on_warning:
                email_data.update({
                    "to": ",".join(send_to),
                })
        elif not send_to:
            errors.append({
                "message": "to missing from email_data",
                "failed": True,
                "key": "to",
            })
    except KeyError:
        errors.append({
            "message": "to missing from email_data",
            "failed": True,
            "key": "to",
        })

    # print ">>> Email Data after 'to' Clean up"
    # print email_data

    try:
        send_cc = email_data["cc"]
        if isinstance(send_cc, list):
            warnings.append({
                "message": "provided a list of cc emails in email_data",
                "failed": False,
                "key": "cc",
            })
            if not fail_on_warning:
                email_data.update({
                    "cc": ",".join(send_cc),
                })
        elif not send_cc:
            warnings.append({
                "message": "bcc emails empty in email_data",
                "failed": fail_on_warning,
                "key": "cc",
            })
    except KeyError:
        warnings.append({
            "message": "cc missing from email_data",
            "failed": False,
            "key": "cc",
        })

    # print ">>> Email Data after 'cc' Clean up"
    # print email_data

    try:
        send_bcc = email_data["bcc"]
        if isinstance(send_bcc, list):
            warnings.append({
                "message": "provided a list of bcc emails in email_data",
                "failed": fail_on_warning,
                "key": "bcc",
            })
            if not fail_on_warning:
                email_data.update({
                    "bcc": ",".join(send_bcc),
                })
        elif not send_bcc:
            warnings.append({
                "message": "bcc emails empty in email_data",
                "failed": fail_on_warning,
                "key": "bcc",
            })
    except KeyError:
        warnings.append({
            "message": "bcc missing from email_data",
            "failed": False,
            "key": "bcc",
        })

    # print ">>> Email Data after 'bcc' Clean up"
    # print email_data

    try:
        send_thread_id = email_data["threadId"]
    except KeyError:
        send_thread_id = ""
        warnings.append({
            "message": "threadId missing from email_data",
            "failed": False,
            "key": "threadId",
        })
        if not fail_on_warning:
            email_data.update({
                "threadId": send_thread_id,
            })

    # print ">>> Email Data after 'thread' Clean up"
    # print email_data

    try:
        send_subject = email_data["subject"]
        if not send_subject:
            warnings.append({
                "message": "subject empty in email_data",
                "failed": fail_on_warning,
                "key": "subject",
            })
    except KeyError:
        warnings.append({
            "message": "subject missing from email_data",
            "failed": fail_on_warning,
            "key": "subject",
        })
        if not fail_on_warning:
            email_data.update({
                "subject": "",
            })

    # print ">>> Email Data after 'subject' Clean up"
    # print email_data

    try:
        send_body = email_data["body"]
        if not send_body:
            warnings.append({
                "message": "body empty in email_data",
                "failed": fail_on_warning,
                "key": "body",
            })
    except KeyError:
        errors.append({
            "message": "body missing from email_data",
            "failed": True,
            "key": "body",
        })

    # print ">>> Email Data after 'body' Clean up"
    # print email_data

    try:
        if email_data["is_attachment"]:
            try:
                file_dir = email_data["file_dir"]
                if not file_dir:
                    errors.append({
                        "message": "file_dir empty in email_data",
                        "failed": True,
                        "key": "file_dir",
                    })
            except KeyError:
                errors.append({
                    "message": "file_dir missing from email_data",
                    "failed": True,
                    "key": "file_dir",
                })
            try:
                filename = email_data["filename"]
                if not filename:
                    errors.append({
                        "message": "filename empty in email_data",
                        "failed": True,
                        "key": "filename",
                    })
            except KeyError:
                errors.append({
                    "message": "filename missing from email_data",
                    "failed": True,
                    "key": "filename",
                })
    except KeyError:
        warnings.append({
            "message": "is_attachment missing from email_data",
            "failed": False,
            "key": "is_attachment",
        })
        email_data.update({
            "is_attachment": False,
        })

    # print ">>> Email Data after 'attachment' Clean up"
    # print email_data

    try:
        if not email_data["is_html"]:
            warnings.append({
                "message": "is_html False in email_data",
                "failed": False,
                "key": "is_html",
            })
    except KeyError:
        warnings.append({
            "message": "is_html missing from email_data",
            "failed": False,
            "key": "is_html",
        })
        email_data.update({
            "is_html": True,
        })

    # print ">>> Email Data after 'is_html' Clean up"
    # print email_data

    success = True

    if errors:
        success = False
    else:
        for warning in warnings:
            if warning["failed"]:
                success = False
                break

    response = {
        "success": success,
        "email_data": email_data,
        "errors": errors,
        "warnings": warnings,
    }

    # print ">>> Response after Adding Errors and Warnings"
    # print response

    return response


def get_attachments(mime_message):

    cid_list = []
    attachments = []

    for part in mime_message.walk():
        if str(part.get_content_type()) == 'text/html':
            soup = BeautifulSoup(part.get_payload(decode=True))
            try:
                cid = '<%s>' % soup('img')[0]['src'][4:]
            except IndexError:
                cid = ''
            cid_list.append(cid)

    for part in mime_message.walk():
        if part.get('Content-ID') in cid_list:
            attachments.append((part.get_filename(), part.get_payload(decode=True)))

    return attachments
