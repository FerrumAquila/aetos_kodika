import os
import json
import datetime
import base64

from django.db import models
from jsonfield import JSONField
from oauth2client.client import AccessTokenRefreshError
from django.core.files import File
from django.conf import settings
from collections import defaultdict

import utils as app_utils


class GmailMessage():

    def __init__(self, message):
        self.raw = message
        self.history_id = message['historyId']
        self.snippet = message['snippet']
        self.thread_id = message['threadId']
        self.labels = [Label.objects.get_or_create(label_id=label) for label in message['labelIds']]
        self.message_id = message['id']
        itemd = defaultdict(list)

        for item in message['payload']['headers']:
            itemd[item['name']].append(item['value'])

        self.headers = itemd
        # TODO: clean to emails, input:
        # [u'Vishesh Bangotra <vishesh@coverfoxmail.com>, Devendra Rane <dev@coverfoxmail.com>']
        # DONE clean_email
        self.to = self.clean_email(self.headers['To'])
        self.from_contact = self.clean_email(self.headers['From'])
        self.cc = self.clean_email(self.headers['Cc'])
        self.subject = self.headers['Subject']
        self.sent_on = datetime.datetime.strptime(
            itemd['Date'][0].split(",")[1].strip().split(' +')[0], "%d %b %Y %H:%M:%S"
        )
        self.body = ""
        self.html = ""
        self.attachments = ""
        self.process_payload(message['payload'])

    def clean_email(self, email_arr):
        emails = [tuple(email.strip(">").split(" <")) for email in email_arr[0].split(", ")]
        return emails

    def process_payload(self, payload):
        if payload['filename']:
            self.process_attachment(payload)
            return
        elif payload['mimeType'].contains('plain'):
            self.process_body(payload)
            return
        elif payload['mimeType'].contains('html'):
            self.process_html(payload)
            return
        else:
            if payload.get('parts') and len(payload['parts']) > 0:
                for payload_parts in payload['parts']:
                    self.process_payload(payload_parts)

    def process_attachment(self, payload):
        # TODO: Implement this code
        self.attachments = "attachment_found"
        pass

    def process_body(self, payload):
        # TODO: Implement this code
        # DONE
        self.body = base64.urlsafe_b64decode(payload["body"]["data"].encode("utf-8"))

    def process_html(self, payload):
        # TODO: Implement this code
        # DONE
        self.html = base64.urlsafe_b64decode(payload["body"]["data"].encode("utf-8"))


class BaseModel(models.Model):
    """
    Each model will have these fields
    """
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Credential(BaseModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    credentials = JSONField()

    def get_credentials(self):
        c = self.credentials
        for j in ['_module', '_class', 'invalid']:
            if j in c:
                c.pop(j)
        return c

    def get_service(self):
        """
        return the service for further api calls using credentials
        """
        try:
            c = self.get_credentials()
            service = app_utils.authenticate_credentials(c)
            return service
        except AccessTokenRefreshError:
            return False

    def fetch_mails(self):
        service = self.get_service()
        for msg_id, message in app_utils.get_unread(service=service):
            mail = Mail.save_message(message=message, user=self.user)
            app_utils.mark_message_read(service=service, msg_id=mail.message_id)


class Label(BaseModel):
    # label_id is gmail's name/id for Label
    label_id = models.CharField(max_length=50, unique=True)
    # app's name for Label
    name = models.CharField(max_length=50, unique=True)
    data = JSONField()

    def __unicode__(self):
        return self.name


class Thread(BaseModel):
    thread_id = models.CharField(max_length=100, unique=True)
    data = JSONField()

    def __init__(self, *args, **kwargs):
        super(BaseModel, self).__init__(*args, **kwargs)
        self.thread = None

    def __unicode__(self):
        return self.thread_id

    def reply(self, body, to, cc, bcc, attachments=None):
        """
        send reply to a thread instance
        body, to, cc, bcc (all required): reply the thread with new email parameters
        send: False will not send the email, only add email to the thread
        """
        # TODO
        # gmail.send_reply(self.thread_id, body, to, cc, bcc, attachments)
        return True


def generate_filename(self, name):
    url = "attachments/%s/%s" % (self.user.username, self.name)
    return url


class Attachment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=200)
    file = models.FileField(upload_to=generate_filename)
    data = JSONField()

    def __unicode__(self):
        return self.name

    def create(self, name, attachment_file):
        self.name = name
        self.file.save('new', File(attachment_file))
        self.save()
        return self


class AddressBook(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    data = JSONField()

    def __unicode__(self):
        return self.name


class Mail(BaseModel):
    # (Model.variable, mime_message_variable, message_dict_variable)
    EMAIL_TYPES = [
        ("to", "To", "TO"),
        ("cc", "Cc", "CC"),
        ("bcc", "Bcc", "BCC"),
        ("from_contact", "From", "FROM"),
    ]

    # When you send email, the response contains thread and labels
    # Attach the same label to the Mail instance
    labels = models.ManyToManyField(Label)
    # We save email only after gmail-api gives response
    message_id = models.CharField(max_length=200, unique=True, null=True)
    thread = models.ForeignKey(Thread, null=True)

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    subject = models.CharField(max_length=100)
    snippet = models.CharField(max_length=200, null=True)
    body = models.TextField(blank=True, null=True)
    to = models.ManyToManyField(AddressBook, related_name="to_emails")
    cc = models.ManyToManyField(AddressBook, related_name="cc_emails")
    bcc = models.ManyToManyField(AddressBook, related_name="bcc_emails")
    from_contact = models.ManyToManyField(AddressBook, related_name="from_emails")
    is_html = models.BooleanField(default=True)
    is_attachment = models.BooleanField(default=False)
    attachments = models.ManyToManyField(Attachment)
    data = JSONField()
    msg_data = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return "Message ID - " + str(self.message_id)

    def add_emails(self, email_add, tag='to'):
        """
        :param email_add: list of tuple of email address and name e.g. [('Vishesh', 'vishesh@gmail.com')]
        :param tag: valid values 'to', 'cc', 'bcc', 'from_contact'
        :returns AddressBook objects relating to this Mail object
        """

        if email_add:
            # print email_add

            if tag not in ['to', 'cc', 'bcc', 'from_contact']:
                raise ValueError('Not a valid tag, valid tags are to, cc, bcc, from_contact')
            else:
                self_email = getattr(self, tag)
            # print "---------------------------------------------"
            # print tag, email_add
            # print "---------------------------------------------"
            if not isinstance(email_add, list):
                email_add = [email_add]
            if not isinstance(email_add[0], tuple):
                email_add = [(email.split("@")[0], email) for email in email_add]

            address_books = []
            for e in email_add:
                add_book_obj, created = AddressBook.objects.get_or_create(email=e[1])
                if created:
                    add_book_obj.name = e[0]
                    add_book_obj.save()
                address_books.append(add_book_obj)

            self_email.add(*address_books)
            return self_email.all()

    def add_labels(self, label_id):
        """
        :param label_id: list of label_ids e.g. ['INBOX', 'UNREAD']
        :return: Label objects relating to this Mail object
        """
        if label_id:
            if isinstance(label_id, str):
                label_id = [label_id]

            label_objs = []
            for label in label_id:
                # print "----------------------------------"
                # print "Label ID - '" + str(label.id) + "'"
                # print "----------------------------------"
                label_obj, created = Label.objects.get_or_create(label_id=label)
                if created:
                    label_obj.name = label
                    label_obj.label_id = label
                    label_obj.save()
                label_objs.append(label_obj)

            self.labels.add(*label_objs)

            return [label.name for label in self.labels.all()]

        else:
            return "Please give Label to add"

    def add_attachments(self, attachments):
        if attachments:
            add_attachments = []
            for name, attachment in attachments:
                with open(r'attachment_%s' % name, 'wb+') as f:
                    add_attachments.append(Attachment().create(name=name, attachment_file=f))

            for attachment in add_attachments:
                self.attachments.add(attachment)
            return [attachment.name for attachment in self.attachments.all()]

        else:
            return "Please give Files to add"

    def init_from_email_data(self, email_data, user):
        """
        :param email_data: dict containing cleaned email data use gmail.utils.clean_email_data to clean email_data
        :param user: user adding the mail
        :return:
        """

        self.user = user
        self.save()

        if Thread.objects.filter(thread_id=email_data["threadId"]):
            thread = Thread.objects.get(thread_id=email_data["threadId"])
            self.thread = thread
        else:
            thread = email_data["threadId"]
            if thread:
                thread = Thread(thread_id=email_data["threadId"])
                thread.save()
                self.thread = thread

        # print ">>>email_data in init_from_email"
        # print email_data
        # print "--------------------------------------------"

        for tag in Mail.EMAIL_TYPES[:3]:
            try:
                emails = email_data[tag[0]]
            except KeyError:
                emails = []
            if not isinstance(emails, list):
                # print ">>>emails is not list"
                # print emails
                # print "--------------------------------------------"
                emails = emails.replace(" ", "").split(",")
            # print ">>>> emails in '" + str(tag[0]) + "' init_from_email_data"
            # print emails
            self.add_emails(email_add=emails, tag=tag[0])

        self.subject = email_data["subject"]
        self.body = email_data["body"]
        self.is_html = email_data["is_html"]
        self.is_attachment = email_data["is_attachment"]
        if self.is_attachment:
            attachment = Attachment.objects.filter(id=email_data["attachment_id"])
            self.attachments.add(*attachment)
        self.save()
        # print "compare_data is just for comparing"
        compare_data = self.init_email_data()
        # print compare_data
        return json.loads(self.data)

    @classmethod
    def save_message(cls, message, user):
        """
        Will return a mail instance after saving the message
        in the model, if messageId already exists, will return
        existing mail instance
        :param message: MimeMessage from gmail api
        :param user: django User
        :return:
        """

        if Mail.objects.filter(message_id=message["id"]):
            mail = Mail.objects.get(message_id=message["id"])
            return mail

        gmail_message = app_utils.GmailMessage(message)

        thread, created = Thread.objects.get_or_create(thread_id=message["threadId"])
        # TODO: Put the save mail in transaction,
        # so that if the processing fails, dummy mail object is
        # not saved
        m = Mail(
            user=user,
            thread=thread,
            subject=gmail_message.get_subject(),
            snippet=message["snippet"],
            message_id=message["id"],
            is_html=True,
            body=gmail_message.html_part.get_payload(),
            msg_data=message.as_string()
        )
        m.save()

        for tag in Mail.EMAIL_TYPES:
            m.add_emails(email_add=gmail_message.get_addresses(tag[1]), tag=tag[0])

        m.add_labels(message["labelIds"])

        return m

    def convert_to_gmail_format(self):
        """
        convert mail object to gmail send format
        """
        if self.thread:
            thread_id = self.thread.thread_id
        else:
            thread_id = ""

        email_data = {}

        for tag in Mail.EMAIL_TYPES:
            email_data.update({
                tag[0]: [address.email for address in getattr(self, tag[0]).all()]
            })

        filename = self.attachments.all()[0].name
        file_dir = os.path.dirname(self.attachments.all()[0].file.path)

        email_data.update({
            "obj_id": self.id,
            "body": self.body,
            "threadId": thread_id,
            "subject": self.subject,
            "is_html": self.is_html,
            "is_attachment": self.is_attachment,
            "filename": filename,
            "file_dir": file_dir,
        })
        return email_data

    # WIP
    def add_attachment(self, filename=None, file_dir=None):
        email_data = json.loads(self.data)
        email_data.update({
            "attachment": {
                "filename": filename,
                "file_dir": file_dir,
            },
            "is_attachment": True,
        })
        self.is_attachment = True
        self.data = json.dumps(email_data)
        self.save()

    def send_mail(self):
        """
        :return: response of gmail.utils.send_mail()
        """
        service = Credential.objects.get(user=self.user).get_service()
        email_data = self.convert_to_gmail_format()
        response = app_utils.send_mail(service=service, email_data=email_data)
        thread_id = response["gmail_response"]["threadId"]
        self.message_id = response["gmail_response"]["id"]
        self.add_emails(self.user.email, "from_contact")
        self.save()
        if not Thread.objects.filter(thread_id=thread_id):
            thread = Thread(thread_id=thread_id)
            thread.save()
            self.thread = thread
        return response

    def save_reply_object(self, reply_email_data, quote=True):
        email_data = json.loads(self.data)
        body = reply_email_data["body"]
        if quote:
            body = \
                unicode("<div dir='ltr'>") + unicode(body) + unicode("</div>") + \
                unicode("<br>----Quote:<br>") + unicode(self.body)

        email_data.update({"body": body})
        email_to =\
            email_data["from_contact"] + email_data["cc"] + email_data["to"] + \
            reply_email_data["to"].replace(" ", "").split(",")
        email_to.remove(self.user.email)

        # print ">>>>email_to just after removing self.user.email"
        # print email_to
        # print "--------------------------------------------"
        email_data.update({"to": email_to})
        mail_obj = Mail()
        mail_obj.init_from_email_data(email_data=email_data, user=self.user)
        return mail_obj
