from django.conf.urls import patterns, url


urlpatterns = patterns(
    'twitter_clone.views',

    # Main urls for viewing the templates
    url(r'^follow/$', 'follow', name='tc_follow'),
    url(r'^follow/(?P<username>\w+)/$', 'follow', name='tc_follow'),
    url(r'^home/$', 'home', name='tc_home'),
    url(r'^landing_page/$', 'landing_page', name='tc_landing_page'),
    url(r'^login/$', 'login', name='tc_login'),
    url(r'^like_tweet/$', 'like_tweet', name='tc_like_tweet'),
    url(r'^like_tweet/(?P<tweet_id>\w+)/$', 'like_tweet', name='tc_like_tweet'),
    url(r'^timeline/(?P<username>\w+)/$', 'timeline', name='tc_timeline'),
    url(r'^timeline/$', 'timeline', name='tc_timeline'),
    url(r'^profile/$', 'profile', name='tc_profile'),
    url(r'^post_tweet/$', 'post_tweet', name='tc_post_tweet'),
    url(r'^reply_tweet/$', 'reply_tweet', name='tc_reply_tweet'),
    url(r'^reply_tweet/(?P<tweet_id>\w+)/$', 'reply_tweet', name='tc_reply_tweet'),
    url(r'^tweet/$', 'tweet', name='tc_tweet'),
    url(r'^tweet/(?P<tweet_id>\w+)/$', 'tweet', name='tc_tweet'),
    url(r'^$', 'home', name='tc_home'),
)
